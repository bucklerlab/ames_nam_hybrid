#!/usr/bin/env bash

input_dir=/workdir/gr226/Ames_NAM_hybrid/data/
output_dir=/workdir/gr226/Ames_NAM_hybrid/data/Ames/

Ames_prefix=${input_dir}Ames/AGPv4_Ames_chr

n_chromosomes=10

declare -a testers=("PHZ51" "B47" "PHZ51_B47")


for tester in "${testers[@]}"
do

###################################################################
# Calculating allele frequencies in Ames
###################################################################
for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}: Calculating allele frequencies...

het_prefix=${output_dir}${tester}/AGPv4_Ames_chr${i}.${tester}-het

sleep 1

vcftools --gzvcf ${Ames_prefix}${i}.augmented.imputed.vcf.gz \
--keep ${input_dir}Ames/inbreds_${tester}.txt \
--positions ${het_prefix}.012.pos \
--out ${output_dir}${tester}/AGPv4_Ames_chr${i}.${tester} \
--freq &

done
wait


###################################################################
# Formatting into single table
###################################################################
output_file=${output_dir}${tester}/Ames_${tester}_v4.frq

echo -e "CHROM\tPOS\tREF\tALT\tAF\tN" > ${output_file}

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}: Formatting...

tail -n +2 ${output_dir}${tester}/AGPv4_Ames_chr${i}.${tester}.frq | awk -F'[\t:]' '{print $1 "\t" $2 "\t" $5 "\t" $7 "\t" $6 "\t" $4}' >> ${output_file}

done

done
wait