#!/usr/bin/env bash

suffix=.bimbam.gz

BSLMM_prefix=BSLMM
RR_prefix=RR

declare -a inputs=("Ames/PHZ51_B47" "NAM/PHZ51")

declare -A files=(["Ames/PHZ51_B47"]="AGPv4_Ames.PHZ51_B47-het${suffix}" ["Ames/PHZ51"]="AGPv4_Ames.PHZ51-het${suffix}" ["Ames/B47"]="AGPv4_Ames.B47-het${suffix}" ["NAM/PHZ51"]="AGPv4_NAM.PHZ51-het${suffix}")

n_phenotypes=3

burnin=1000000
sampling=10000000


###################################################################
# Fitting BSLMM
###################################################################
echo Fitting BSLMM...


for j in $(seq 1 1 ${n_phenotypes})
do


echo Trait ${j}


for input in "${inputs[@]}"
do


echo Input: ${input}

file=${files[${input}]}

input_dir=/workdir/gr226/Ames_NAM_hybrid/data/${input}/


if [ -d "${input_dir}" ]
then


cd ${input_dir}


# Additive effects: RR fit
gemma \
-g ${file} \
-n ${j} \
-p ${input_dir}Y.bimbam \
-bslmm 2 \
-notsnp \
-w ${burnin} \
-s ${sampling} \
-o ${RR_prefix}-${j}


# Additive effects: BSLMM fit
gemma \
-g ${file} \
-n ${j} \
-p ${input_dir}Y.bimbam \
-bslmm 1 \
-seed 1 \
-notsnp \
-w ${burnin} \
-s ${sampling} \
-o ${BSLMM_prefix}-${j}


fi


done


wait


done


wait


###################################################################
# Formatting BSLMM output
###################################################################
echo Formatting BSLMM output...


for input in "${inputs[@]}"
do


echo Input: ${input}


input_dir=/workdir/gr226/Ames_NAM_hybrid/data/${input}/


if [ -d "${input_dir}" ]
then


cd ${input_dir}/output


# Breeding values
paste -d '\t' ${RR_prefix}-?.bv.txt > ${input_dir}${RR_prefix}-bv.txt

paste -d '\t' ${BSLMM_prefix}-?.bv.txt > ${input_dir}${BSLMM_prefix}-bv.txt


# Association results
echo -e "trait\trs\talpha" > ${input_dir}${RR_prefix}.txt

echo -e "trait\trs\talpha\tbeta\tgamma" > ${input_dir}${BSLMM_prefix}.txt


for j in $(seq 1 1 ${n_phenotypes})
do

echo Trait ${j}

tail -n +2 ${RR_prefix}-${j}.param.txt | awk -F'\t' '{print $2 "\t" $5}' | sed "s/^/${j}\t/" >> ${input_dir}${RR_prefix}.txt

tail -n +2 ${BSLMM_prefix}-${j}.param.txt | awk -F'\t' '{print $2 "\t" $5 "\t" $6 "\t" $7}' | sed "s/^/${j}\t/" >> ${input_dir}${BSLMM_prefix}.txt

done


fi


done
