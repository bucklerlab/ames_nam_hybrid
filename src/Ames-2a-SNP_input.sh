#!/usr/bin/env bash

input_dir=/workdir/gr226/omic_prediction/data/
output_dir=/workdir/gr226/Ames_NAM_hybrid/data/

n_chromosomes=10

checkpoint=1

if [ ! -d ${output_dir}tmp/ ]
then
mkdir ${output_dir}tmp/
fi

cd ${output_dir}tmp/


###################################################################
# 1. Converting input to VCF by chromosome
###################################################################
if [ ${checkpoint} -le 1 ]
then

echo Converting...

# Converting
~/tassel-5-standalone/run_pipeline.pl -Xmx200g -h5 ${input_dir}Ames/ZeaGBSv27_20171204_AGPv4_Ames.h5 \
-separate -sortPositions \
-export ${output_dir}Ames/AGPv4_Ames_chr -exportType VCF

fi

###################################################################
# 2. Indexing VCF
###################################################################
if [ ${checkpoint} -le 2 ]
then

echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

# Indexing
input_Ames=${output_dir}Ames/AGPv4_Ames_chr${i}

bgzip -f ${input_Ames}.vcf
tabix -f -p vcf ${input_Ames}.vcf.gz

done

fi

