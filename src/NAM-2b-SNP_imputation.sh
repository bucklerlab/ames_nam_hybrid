#!/usr/bin/env bash

home_dir=/workdir/gr226/Ames_NAM_hybrid/
cd ${home_dir}

input_dir=${home_dir}data/

input_prefix=${input_dir}NAM/ZeaGBSv27_AGPv4_NAM_chr
output_prefix=${input_dir}NAM/AGPv4_NAM_chr

Hmp_prefix=${input_dir}Hmp321/hmp321_282_agpv4_merged_chr
ref_prefix=${input_dir}NAM/AGPv4_parents_chr

beagle_version=beagle.03Jul18.40b.jar

n_chromosomes=10

checkpoint=1


###################################################################
# 1. Format data in NAM
###################################################################
if [ ${checkpoint} -le 1 ]
then

# Filtering markers
echo Filtering markers in NAM...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools annotate -x ^FORMAT/GT -e 'REF~"N" | ALT~"N"' -Ov -o ${input_prefix}${i}.vcf ${input_prefix}${i}.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

sed -i '/^#/! s/0\/1/\.\/\./g; s/1\/0/\.\/\./g' ${input_prefix}${i}.vcf &

done
wait

# Progeny list
grep Z0 ${input_dir}NAM/GBS_names.txt > ${input_dir}NAM/GBS_progeny.txt

# Subsetting
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --vcf ${input_prefix}${i}.vcf \
--out ${input_prefix}${i} \
--positions ${Hmp_prefix}${i}.imputed.pos \
--keep ${input_dir}NAM/GBS_progeny.txt \
--min-alleles 2 \
--max-alleles 2 \
--recode &

done
wait

# Indexing
echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${input_prefix}${i}.recode.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${input_prefix}${i}.recode.vcf.gz &

done
wait

fi


###################################################################
# 2. Subsetting WGS Hapmap 321 on NAM parents
###################################################################
if [ ${checkpoint} -le 2 ]
then

echo Subsetting Hapmap 321 on NAM parents...

# Parent list
cat ${input_dir}NAM/parents/*.txt | sort | uniq > ${input_dir}Hmp321/NAM_parents.txt

# Subset GBS NAM data on progeny
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --gzvcf ${Hmp_prefix}${i}.imputed.vcf.gz \
--out ${ref_prefix}${i} \
--keep ${input_dir}Hmp321/NAM_parents.txt \
--recode &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${ref_prefix}${i}.recode.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${ref_prefix}${i}.recode.vcf.gz &

done
wait

fi


###################################################################
# 3. Imputation in NAM
###################################################################
if [ ${checkpoint} -le 3 ]
then

echo Imputing in NAM...

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}

ref=${ref_prefix}${i}.recode
study=${input_prefix}${i}.recode
out=${output_prefix}${i}.imputed

java -Xmx120g -jar ${beagle_version} ref=${ref}.vcf.gz gt=${study}.vcf.gz burnin=10 iterations=15 ne=1000 out=${out}

tabix -f -p vcf ${out}.vcf.gz &

done

fi
