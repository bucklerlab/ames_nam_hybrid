#!/usr/bin/env bash

input_dir=/workdir/gr226/Ames_NAM_hybrid/data/
output_dir=/workdir/gr226/Ames_NAM_hybrid/data/NAM/

NAM_prefix=${input_dir}NAM/AGPv4_NAM_chr

checkpoint=1

n_chromosomes=10

declare -a testers=("PHZ51")

declare -a families=("Z001" "Z002" "Z003" "Z004" "Z005" "Z006" "Z007" "Z008" "Z009" "Z011" "Z012" "Z013" "Z014" "Z015" "Z016" "Z018" "Z019" "Z020" "Z021" "Z022" "Z023" "Z024" "Z025" "Z026")

for tester in "${testers[@]}"
do

###################################################################
# 1. Calculating allele frequencies in NAM
###################################################################
if [ ${checkpoint} -le 1 ]
then

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

echo Chromosome ${i}: Calculating allele frequencies...

het_prefix=${output_dir}${tester}/AGPv4_NAM_chr${i}.${tester}-het

vcftools --gzvcf ${NAM_prefix}${i}.imputed.vcf.gz \
--keep ${input_dir}NAM/inbreds_${tester}.txt \
--positions ${het_prefix}.012.pos \
--out ${output_dir}${tester}/AGPv4_NAM_chr${i}.${tester} \
--freq &

done
wait

fi


###################################################################
# 2. Formatting allele frequencies in NAM into single table
###################################################################
if [ ${checkpoint} -le 2 ]
then

output_file=${output_dir}${tester}/NAM_${tester}_v4.frq

echo -e "CHROM\tPOS\tREF\tALT\tAF\tN" > ${output_file}

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}: Formatting...

tail -n +2 ${output_dir}${tester}/AGPv4_NAM_chr${i}.${tester}.frq | awk -F'[\t:]' '{print $1 "\t" $2 "\t" $5 "\t" $7 "\t" $6 "\t" $4}' >> ${output_file}

done

fi

###################################################################
# 3. Calculating allele frequencies by family
###################################################################
if [ ${checkpoint} -le 3 ]
then

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}: Calculating allele frequencies by family...

het_prefix=${output_dir}${tester}/AGPv4_NAM_chr${i}.${tester}-het

for family in "${families[@]}"
do

sleep 1

grep ${family} ${input_dir}NAM/inbreds_${tester}.txt > ${family}_members.txt

vcftools --gzvcf ${NAM_prefix}${i}.imputed.vcf.gz \
--keep ${family}_members.txt \
--positions ${het_prefix}.012.pos \
--out ${output_dir}${tester}/AGPv4_NAM_chr${i}.${tester}.${family} \
--freq &

done
wait

done
wait

for family in "${families[@]}"; do rm ${family}_members.txt; done

fi

###################################################################
# 4. Formatting allele frequencies by family into single table
###################################################################
if [ ${checkpoint} -le 4 ]
then

output_file=${output_dir}${tester}/NAM_${tester}_v4_families.frq

echo -e "FAMILY\tCHROM\tPOS\tREF\tALT\tAF\tN" > ${output_file}

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}: Formatting...

for family in "${families[@]}"
do

tail -n +2 ${output_dir}${tester}/AGPv4_NAM_chr${i}.${tester}.${family}.frq | awk -F'[\t:]' '{print $1 "\t" $2 "\t" $5 "\t" $7 "\t" $6 "\t" $4}' | sed "s/^/${family}\t/" >> ${output_file}

done

done

fi

done
