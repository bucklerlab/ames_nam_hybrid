# README #

Functional and genomic basis for heterosis in diverse maize populations

### Project ###

* __Tentative title__: The relevance of dominance and functional annotations to predict agronomic traits in hybrid maize
* __Summary__: This study will consist of dissecting the genetic architecture of hybrid traits in two maize populations and assess the potential of such models for improving cross-population prediction

### Content ###

* _src/_: Bash and R code on data processing and analysis
* _doc/_: Manuscript drafts
* _figs/_: Manuscript figures
* _notebook/_: Notebooks and virtual environments (if any)

### Contacts ###

* Owners: Guillaume Ramstein (gr226@cornell.edu), M. Cinta Romay (mcr72@cornell.edu)
* Other contacts: Ed Buckler, Sara Miller, Cinta Romay (Buckler lab)
