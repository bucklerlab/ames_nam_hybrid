#!/usr/bin/env bash

input_dir=/workdir/gr226/Ames_NAM_hybrid/data/NAM/
input_prefix=${input_dir}AGPv4_NAM_chr

Hmp_prefix=/workdir/gr226/Ames_NAM_hybrid/data/Hmp321/hmp321_282_agpv4_merged_chr

n_chromosomes=10

checkpoint=1

declare -a testers=("PHZ51")

###################################################################
# Calling hybrid genotypes by tester
###################################################################
for tester in "${testers[@]}"

do

echo -e "\nCalling hybrid SNPs for ${tester} hybrids\n\n"

# Directories
output_dir=${input_dir}${tester}/
mkdir ${output_dir}

output_prefix=${output_dir}AGPv4_NAM_chr
tester_prefix=${output_dir}${tester}_v4_chr

# List of testers
tester_pattern=$(echo ${tester} | sed "s/_/|/")
echo ${tester_pattern} | sed "s/|/\n/" > ${tester}_list.txt

# Making hybrid file
grep -E "${tester_pattern}" ${input_dir}hybrids.txt | awk -F"\t" '{print $2 "\t" $1}' > ${input_dir}hybrids_${tester}.txt
grep -E "${tester_pattern}" ${input_dir}hybrids.txt | awk -F"\t" '{print $2}' > ${input_dir}inbreds_${tester}.txt


###################################################################
# 1. Selecting sites
###################################################################
if [ ${checkpoint} -le 1 ]
then

echo Selecting sites for filtering...
for i in $(seq 1 1 ${n_chromosomes})
do

bcftools query -f '%CHROM %POS\n' ${input_prefix}${i}.imputed.vcf.gz | sort | uniq -u > ${output_dir}/non_duplicates_NAM_chr${i}.pos

vcftools --gzvcf ${input_prefix}${i}.imputed.vcf.gz \
--out ${output_prefix}${i} \
--remove-indels \
--keep ${input_dir}inbreds_${tester}.txt \
--positions ${output_dir}/non_duplicates_NAM_chr${i}.pos \
--max-missing 0.8 \
--min-alleles 2 \
--max-alleles 2 \
--maf 0.01 \
--kept-sites &

bcftools query -f '%CHROM %POS\n' ${Hmp_prefix}${i}.imputed.vcf.gz | sort | uniq -u > ${output_dir}/non_duplicates_${tester}_chr${i}.pos

vcftools --gzvcf ${Hmp_prefix}${i}.imputed.vcf.gz \
--out ${tester_prefix}${i} \
--remove-indels \
--keep ${tester}_list.txt \
--positions ${output_dir}/non_duplicates_${tester}_chr${i}.pos \
--max-missing 1.0 \
--remove-indels \
--min-alleles 2 \
--max-alleles 2 \
--kept-sites &

done
wait


for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

cut -f2 ${output_prefix}${i}.kept.sites | tail -n +2 | sort | uniq -u > ${output_dir}/NAM_chr${i}.pos &

sleep 1

cut -f2 ${tester_prefix}${i}.kept.sites | tail -n +2 | sort | uniq -u > ${output_dir}/${tester}_chr${i}.pos &

done
wait


for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

comm -12 ${output_dir}/NAM_chr${i}.pos ${output_dir}/${tester}_chr${i}.pos | sed "s/^/${i}\t/" > ${output_dir}/filtered_sites_chr${i}_${tester} &

done
wait


fi


###################################################################
# 2. Filtering VCF files
###################################################################
if [ ${checkpoint} -le 2 ]
then

echo Filtering...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --gzvcf ${input_prefix}${i}.imputed.vcf.gz \
--out ${output_prefix}${i}.${tester}-filtered \
--keep ${input_dir}inbreds_${tester}.txt \
--positions ${output_dir}/filtered_sites_chr${i}_${tester} \
--recode &

sleep 1

vcftools --gzvcf ${Hmp_prefix}${i}.imputed.vcf.gz \
--out ${tester_prefix}${i} \
--keep ${tester}_list.txt \
--positions ${output_dir}/filtered_sites_chr${i}_${tester} \
--recode &

done
wait


fi


###################################################################
# 3. Processing VCF files
###################################################################
if [ ${checkpoint} -le 3 ]
then

echo Compressing...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${output_prefix}${i}.${tester}-filtered.recode.vcf &

sleep 1

bgzip -f ${tester_prefix}${i}.recode.vcf &

done
wait


echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${output_prefix}${i}.${tester}-filtered.recode.vcf.gz &

sleep 1

tabix -f -p vcf ${tester_prefix}${i}.recode.vcf.gz &

done
wait


fi


###################################################################
# 4. Tester file in GT format
###################################################################
if [ ${checkpoint} -le 4 ]
then

echo Reformatting tester files...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools annotate --threads 1 -x ^FORMAT/GT -Ov -o ${tester_prefix}${i}.recode-GT.vcf ${tester_prefix}${i}.recode.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

sed -i '/^#/! s/0|1/\.|\./g; s/1|0/\.|\./g' ${tester_prefix}${i}.recode-GT.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${tester_prefix}${i}.recode.vcf.gz; done


echo Indexing reformatted files...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${tester_prefix}${i}.recode-GT.vcf &

done
wait


for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${tester_prefix}${i}.recode-GT.vcf.gz &

done
wait


fi


###################################################################
# 5. Merging inbred and tester data
###################################################################
if [ ${checkpoint} -le 5 ]
then

echo Merging inbred and tester data...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools merge -m all -Oz -o ${output_prefix}${i}.${tester}-merged-unfiltered.vcf.gz --threads 4 ${output_prefix}${i}.${tester}-filtered.recode.vcf.gz ${tester_prefix}${i}.recode-GT.vcf.gz &

done
wait


echo Normalizing after merging...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools norm -m +any --threads 1 -Oz -o ${output_prefix}${i}.${tester}-merged-normalized.vcf.gz ${output_prefix}${i}.${tester}-merged-unfiltered.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${output_prefix}${i}.${tester}-merged-unfiltered.vcf.gz; done


echo Filtering after merging...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools view -m2 -M2 -v snps --threads 1 -Oz -o ${output_prefix}${i}.${tester}-merged-unsorted.vcf.gz ${output_prefix}${i}.${tester}-merged-normalized.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${output_prefix}${i}.${tester}-merged-normalized.vcf.gz; done


echo Sorting after merging...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools sort -Oz -o ${output_prefix}${i}.${tester}-merged.vcf.gz ${output_prefix}${i}.${tester}-merged-unsorted.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${output_prefix}${i}.${tester}-merged-unsorted.vcf.gz; done


fi


###################################################################
# 6. Making hybrid data
###################################################################
if [ ${checkpoint} -le 6 ]
then

echo Making hybrid genotypes...

for i in $(seq 1 1 ${n_chromosomes})
do

het_prefix=${output_dir}AGPv4_NAM_chr${i}.${tester}-het

~/tassel-5-standalone/run_pipeline.pl -Xmx200g -vcf ${output_prefix}${i}.${tester}-merged.vcf.gz \
-CreateHybridGenotypesPlugin -hybridFile ${input_dir}hybrids_${tester}.txt -endPlugin \
-export ${het_prefix} -exportType VCF

done


echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

het_prefix=${output_dir}AGPv4_NAM_chr${i}.${tester}-het

bgzip -f ${het_prefix}.vcf

tabix -f -p vcf ${het_prefix}.vcf.gz &

done
wait


fi


###################################################################
# 7. Converting to 012 format
###################################################################
if [ ${checkpoint} -le 7 ]
then

echo Converting to 012 format...

for i in $(seq 1 1 ${n_chromosomes})
do

het_prefix=${output_dir}AGPv4_NAM_chr${i}.${tester}-het

sleep 1

vcftools --gzvcf ${het_prefix}.vcf.gz \
--out ${het_prefix} \
--max-missing 0.8 \
--012 &

done
wait


fi

done

