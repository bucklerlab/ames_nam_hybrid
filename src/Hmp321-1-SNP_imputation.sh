#!/usr/bin/env bash

home_dir=/workdir/gr226/Ames_NAM_hybrid/
input_dir=${home_dir}data/

in_prefix=${input_dir}Hmp321/hmp321_agpv4_chr
Hmp_prefix=${input_dir}Hmp321/hmp321_without282set_agpv4_chr
ref_prefix=${input_dir}Hmp321/hmp321_282_agpv4_merged_chr

tester_prefix=${input_dir}Hmp321/testers_agpv4_chr

G282_prefix=${input_dir}G282/hmp321_agpv4_chr

beagle_version=beagle.03Jul18.40b.jar

n_chromosomes=10

checkpoint=6

if [ ! -d ${home_dir}tmp ]; then mkdir ${home_dir}tmp; fi
cd ${home_dir}tmp


###################################################################
# 1. Format data in Hapmap321
###################################################################
if [ ${checkpoint} -le 1 ]
then

# Discarding 282 samples
echo Discarding 282 samples...

bcftools query -l ${in_prefix}1.vcf.gz | grep -v 282set > to_keep.txt

for i in $(seq 1 1 ${n_chromosomes})
do

bcftools annotate -x ^FORMAT/GT -Ov -o ${in_prefix}${i}.vcf ${in_prefix}${i}.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sed -i '/^#/! s/0\/1/\.\/\./g; s/1\/0/\.\/\./g' ${in_prefix}${i}.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

vcftools --vcf ${in_prefix}${i}.vcf \
--out ${Hmp_prefix}${i} \
--keep to_keep.txt \
--remove-indels \
--max-missing 0.5 \
--min-alleles 2 \
--max-alleles 2 \
--mac 3 \
--recode &

done
wait

# Indexing
echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do
bgzip -f ${Hmp_prefix}${i}.recode.vcf &
done
wait

for i in $(seq 1 1 ${n_chromosomes})
do
tabix -f -p vcf ${Hmp_prefix}${i}.recode.vcf.gz &
done
wait

fi


###################################################################
# 2. Format data in 282
###################################################################
if [ ${checkpoint} -le 2 ]
then

# Filtering markers
echo Filtering markers in 282...

for i in $(seq 1 1 ${n_chromosomes})
do
bcftools query -f '%CHROM\t%POS\n' ${Hmp_prefix}${i}.recode.vcf.gz > hmp321_agpv4_chr${i}.recode.pos &
done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

bcftools annotate -x ^FORMAT/GT -Ov -o ${G282_prefix}${i}.vcf ${G282_prefix}${i}.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sed -i '/^#/! s/0\/1/\.\/\./g; s/1\/0/\.\/\./g' ${G282_prefix}${i}.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

vcftools --vcf ${G282_prefix}${i}.vcf \
--out ${G282_prefix}${i} \
--positions hmp321_agpv4_chr${i}.recode.pos \
--recode &

done
wait

# Indexing
echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

bgzip -f ${G282_prefix}${i}.recode.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

tabix -f -p vcf ${G282_prefix}${i}.recode.vcf.gz &

done
wait

fi


###################################################################
# 3. Reference panel: 282 samples + Hapmap321
###################################################################
if [ ${checkpoint} -le 3 ]
then

echo Merging 282 and Hapmap321...

# Merging
for i in $(seq 1 1 ${n_chromosomes})
do

bcftools merge -m all -Oz -o ${ref_prefix}${i}-unfiltered.vcf.gz --threads 10 \
${Hmp_prefix}${i}.recode.vcf.gz ${G282_prefix}${i}.recode.vcf.gz &

done
wait

# Normalizing
for i in $(seq 1 1 ${n_chromosomes})
do
bcftools norm -m +any -Oz -o ${ref_prefix}${i}-normalized.vcf.gz ${ref_prefix}${i}-unfiltered.vcf.gz &
done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${ref_prefix}${i}-unfiltered.vcf.gz; done

# Filtering
for i in $(seq 1 1 ${n_chromosomes})
do
bcftools view -m2 -M2 -v snps -Oz -o ${ref_prefix}${i}-unsorted.vcf.gz ${ref_prefix}${i}-normalized.vcf.gz &
done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${ref_prefix}${i}-normalized.vcf.gz; done

# Sorting
for i in $(seq 1 1 ${n_chromosomes})
do
bcftools sort -Oz -o ${ref_prefix}${i}.vcf.gz ${ref_prefix}${i}-unsorted.vcf.gz &
done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${ref_prefix}${i}-unsorted.vcf.gz; done

fi


###################################################################
# 4. Imputation in Hapmap3.2.1
###################################################################
cd ${home_dir}

if [ ${checkpoint} -le 4 ]
then

echo Imputing in Hapmap321...

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}

sample=${ref_prefix}${i}
ref=${ref_prefix}${i}.imputed

java -Xmx500g -jar ${beagle_version} gt=${sample}.vcf.gz burnin=10 iterations=15 ne=1000 out=${ref}

done

fi

###################################################################
# 5. Marker positions in Hapmap3.2.1
###################################################################
if [ ${checkpoint} -le 5 ]
then

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools query -f '%CHROM\t%POS\n' ${ref_prefix}${i}.imputed.vcf.gz > ${ref_prefix}${i}.imputed.pos &

done
wait

fi


###################################################################
# 6. Tester genotypes
###################################################################
if [ ${checkpoint} -le 6 ]
then

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --gzvcf ${ref_prefix}${i}.imputed.vcf.gz \
--out ${tester_prefix}${i} \
--remove-indels \
--indv B47 \
--indv PHZ51 \
--max-missing 1.0 \
--min-alleles 2 \
--max-alleles 2 \
--012 &

done
wait

fi
