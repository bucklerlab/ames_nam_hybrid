#!/usr/bin/env bash

declare -a panels=("Ames" "NAM")
declare -a testers=("PHZ51_B47" "PHZ51" "B47")

###################################################################
# Formatting GEMMA input
###################################################################
for panel in "${panels[@]}"
do


echo Panel: ${panel}


for tester in "${testers[@]}"
do


echo Tester: ${tester}

input_dir=/workdir/gr226/Ames_NAM_hybrid/data/${panel}/${tester}/


if [ -d "${input_dir}" ]
then


# GEMMA input
if [ -f "${input_dir}AGPv4_${panel}.${tester}-het.bimbam" ]
then

gzip -f ${input_dir}AGPv4_${panel}.${tester}-het.bimbam &


fi


fi


done


done
wait

