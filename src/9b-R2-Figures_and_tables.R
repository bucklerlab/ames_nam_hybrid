# Title     : Revision: genomic prediction models
# Created by: Guillaume Ramstein (gr226@cornell.edu)
# Created on: 7/13/18

#########################################################
# Script parameters
#########################################################
# Working directory
if (Sys.info()["nodename"] == "RS-BTBD1NGR226") {
  wd <- "/media/gr226/Backup"
  src_file <- "/home/gr226/Documents/Ames_NAM_hybrid/ames_nam_hybrid/src/_utils.R"
} else {
  wd <- "/workdir/gr226"
  src_file <- "/workdir/gr226/Ames_NAM_hybrid/src/_utils.R"
}

dir.create(wd, showWarnings=FALSE)
setwd(wd)

# Graphical parameters
u <- 1800
base_size <- 22

# Bin names
feature_legend <- c(
  "additivity"="Additivity",
  "unpartitioned"="Unpartitioned",
  "dominance"="Dominance",
  "epistasis"="Epistasis",
  "AA"="Add.xAdd.",
  "AD"="Add.xDom.",
  "DD"="Dom.xDom.",
  "gene"="Gene",
  "maf"="MAF",
  "r"="Rec.",
  "gerp"="GERP",
  "MNase_HS"="MNase HS",
  "maf+gene"="Gene+MAF",
  "r+gene"="Gene+Rec.",
  "gerp+gene"="Gene+GERP",
  "MNase_HS+gene"="Gene+MNase HS",
  "F"="Genomic inbreeding\n(linear only)",
  "F.quadratic"="Genomic inbreeding",
  "GWAS"="GWAS"
)

bin_legend <- c(
  "base"="Add.",
  "base_D"="Dom.",
  "base_AA"="Add.xAdd.",
  "base_AD"="Add.xDom.",
  "base_DD"="Dom.xDom.",
  
  "gene.bin0"="Gene: Distal",
  "gene.bin1"="Gene: Proximal",
  "maf.bin1"="MAF: < 0.01",
  "maf.bin2"="MAF: (0.01, 0.05)",
  "maf.bin3"="MAF: > 0.05",
  "r.bin1"="Rec.: < 0.45 cM",
  "r.bin2"="Rec.: (0.45, 1.65)",
  "r.bin3"="Rec.: > 1.65 cM",
  "gerp.bin0"="GERP: 0",
  "gerp.bin1"="GERP: > 0",
  "mnase.bin0"="MNase: Dense",
  "mnase.bin1"="MNase: Open",
  
  "gene.bin0_D"="Gene: Distal (D)",
  "gene.bin1_D"="Gene: Proximal (D)",
  "maf.bin1_D"="MAF: < 0.01 (D)",
  "maf.bin2_D"="MAF: (0.01, 0.05) (D)",
  "maf.bin3_D"="MAF: > 0.05 (D)",
  "r.bin1_D"="Rec.: < 0.45 cM (D)",
  "r.bin2_D"="Rec.: (0.45,1.65) (D)",
  "r.bin3_D"="Rec.: > 1.65 cM (D)",
  "gerp.bin0_D"="GERP: 0 (D)",
  "gerp.bin1_D"="GERP: > 0 (D)",
  "mnase.bin0_D"="MNase: Dense (D)",
  "mnase.bin1_D"="MNase: Open (D)",
  
  "F.Endelman"="F",
  "F2.Endelman"="F (Quad.)",
  
  "error"="Error"
)

# Estimate marker effects?
get_marker_effects <- TRUE

# Input data
panel_names <- c("Ames/PHZ51_B47"="Ames-H",
                 "Ames/PHZ51+B47"="Ames-H",
                 "NAM/PHZ51"="NAM-H")
panel_labeller <- function (variable, value) return(panel_names[as.character(value)])

set_names <- c("Ames/PHZ51"="Ames x PHZ51",
               "Ames/B47"="Ames x B47",
               "NAM/PHZ51"="NAM x PHZ51")
set_labeller <- function (variable, value) return(set_names[as.character(value)])

traits <- c("DTS", "PH", "GY")

input_folders <- c("Ames"="Ames_NAM_hybrid/data/Ames/PHZ51_B47",
                   "NAM"="Ames_NAM_hybrid/data/NAM/PHZ51",
                   "Ames/PHZ51_B47"="Ames_NAM_hybrid/data/Ames/PHZ51_B47",
                   "Ames/PHZ51"="Ames_NAM_hybrid/data/Ames/PHZ51",
                   "Ames/B47"="Ames_NAM_hybrid/data/Ames/B47",
                   "NAM/PHZ51"="Ames_NAM_hybrid/data/NAM/PHZ51")

colours <- c(
  "B47"="#0099FF",
  "PHZ51"="#339900",
  "NAM"="#FF3300",
  "Ames"="#330099",
  "NAM/PHZ51"="#FF3300",
  "Ames/PHZ51"="#339900",
  "Ames/B47"="#0099FF",
  "Ames/PHZ51_B47"="#330099",
  "add_only"="#0066FF",
  "add"="#0066FF",
  "dom"="#000022",
  "Tester"="#000000",
  "Female"="#0099FF",
  "Hybrid"="#0033FF"
  )

shapes <- c(
  "Tester"=13,
  "Female"=20,
  "Hybrid"=1
)

sizes <- c(
  "Tester"=6,
  "Female"=1,
  "Hybrid"=2.5
)

alphas <- c(
  "Tester"=1,
  "Female"=0.5,
  "Hybrid"=1
)

popInfo_file <- "Ames_NAM_hybrid/data/popInfo.rds"

GRM_file <- "Ames_NAM_hybrid/data/GRM_list.rds"

GRM.282_file <- "Ames_NAM_hybrid/data/282set/GRM_list.rds"

m_file <- "Ames_NAM_hybrid/data/m_list.rds"

annot_file <- "Ames_NAM_hybrid/data/annot.rds"

n_PCs <- 3

LD_type <- "Q+K"

stratified_sampling <- TRUE

n_folds <- 10

# Tropical NAM Founders (McMullen et al. 2009)
tropical_founders <- c("CML103", "CML228", "CML247", "CML277", "CML322", "CML333", "CML52", "CML69", "Ki11", "Ki3", "NC350", "NC358", "Tzi8")

# p-value format
n_digits <- 3
p.classes <- c(0, 0.001, 0.01, 0.05, 0.1, 1)
p.labels <- c("(0,0.001]"="***", "(0.001,0.01]"="**", "(0.01,0.05]"="*", "(0.05,0.1]"="°", "(0.1,1]"="")

# Output directory
make_FS4 <- FALSE

output_dir <-  ifelse(Sys.info()["nodename"] == "RS-BTBD1NGR226",
                      "/home/gr226/Projects/Ames_NAM_hybrid/figs/revision",
                      "/workdir/gr226/Ames_NAM_hybrid/figs/revision")

dir.create(output_dir, showWarnings=FALSE)

###############################################################################
# Functions
###############################################################################
library(data.table)
library(ggplot2)
library(regress)
library(foreach)
library(mgcv)
library(plyr)
library(reshape2)
library(ggrepel)
library(coefplot)
library(gridExtra)
library(ggsignif)
library(ggcorrplot)
library(GGally)
library(metap)
library(ggpubr)

source(src_file)

###############################################################################
# Marker data
###############################################################################
# Population information
popInfo <- readRDS(popInfo_file)
n <- nrow(popInfo)

popInfo$cl <- NA
popInfo$tester <- gsub(".+/", "", rownames(popInfo))

# GRM
GRM_list <- readRDS(GRM_file)
GRM_names <- names(GRM_list)[grep("_D", names(GRM_list), invert=TRUE)]
stopifnot(all(sapply(GRM_list, function(GRM) all(rownames(popInfo) == rownames(GRM)))))

m <- unlist(readRDS(m_file))

# Epistatic relationship matrices
for (bin in c("base", "gene.bin0", "gene.bin1")) {
  
  GRM_list[[paste(bin, "AA", sep="_")]] <- GRM_list[[bin]] * GRM_list[[bin]]
  GRM_list[[paste(bin, "AD", sep="_")]] <- GRM_list[[bin]] * GRM_list[[paste(bin, "D", sep="_")]]
  GRM_list[[paste(bin, "DD", sep="_")]] <- GRM_list[[paste(bin, "D", sep="_")]] * GRM_list[[paste(bin, "D", sep="_")]]
  
  m[[paste(bin, "AA", sep="_")]] <- m[[bin]]
  m[[paste(bin, "AD", sep="_")]] <- m[[bin]]
  m[[paste(bin, "DD", sep="_")]] <- m[[bin]]
  
}

names(m) <- bin_legend[names(m)]

# Diagonal values in adjusted genomic relationships
Q <- cbind("(Intercept)"=1, as.matrix(popInfo[, paste0("PC", 1:n_PCs)]))
H <- diag(n) - Q %*% solve(crossprod(Q)) %*% t(Q)

D <- lapply(GRM_list, function(GRM) {
  diag(H %*% GRM %*% t(H))
})
D$error <- 1

names(D) <- bin_legend[names(D)]

# SNP map
annot <- readRDS(annot_file)

SNPs <- rownames(annot)

#------------------------------------------------------------------------------
# PCA in 282 set
#------------------------------------------------------------------------------
# GRM
GRM.282_list <- readRDS(GRM.282_file)

GRM.282 <- GRM.282_list$grm
rownames(GRM.282) <- colnames(GRM.282) <- GRM.282_list$sample.id

# PCA
PCA.282 <- eigen(GRM.282)
prop_var <- paste0(round(100*PCA.282$values/sum(PCA.282$values), 1), "%")

###############################################################################
# Validation folds
###############################################################################
fold_list <- list()
Ames_sets <- c("Ames/PHZ51", "Ames/B47")

# NAM/PHZ51
Y.TS_file <- paste(input_folders["NAM/PHZ51"], "Y.rds", sep="/")
TS <- rownames(readRDS(Y.TS_file))

fold_list[["NAM/PHZ51"]] <- split(TS, as.factor(gsub("E.+$", "", TS)))

# Ames/PHZ51+B47
fold_list[["Ames/PHZ51_B47"]] <- replicate(n_folds, c())

for (set in Ames_sets) {
  
  Y.TS_file <- paste(input_folders[set], "Y.rds", sep="/")
  TS <- rownames(readRDS(Y.TS_file))
  
  # Fold assignment
  set.seed(1)
  
  if (stratified_sampling) {
    
    Q.TS <- popInfo[TS, paste0("PC", 1:n_PCs)]
    kmc <- kmeans(Q.TS, centers=4, nstart=1000, iter.max=100)
    
    cluster_name <- setNames(
      LETTERS[1:4],
      c(which.min(kmc$centers[, 1]), apply(kmc$centers, 2, which.max))
    )
    
    clusters <- cluster_name[as.character(kmc$cluster)]

  } else {
    
    clusters <- numeric(length(TS))
    
  }
  
  popInfo[TS, "cl"] <- clusters
  
  for (cl in unique(clusters)) {
    
    TS.cl <- TS[clusters == cl]
    
    TS_split <- split(sample(TS.cl), cut_number(1:length(TS.cl), n_folds))
    
    for (k in 1:n_folds) {
      
      fold_list[["Ames/PHZ51_B47"]][[k]] <- c(fold_list[["Ames/PHZ51_B47"]][[k]], TS_split[[k]])
      
    }
    
  }
  
}

names(fold_list[["Ames/PHZ51_B47"]]) <- paste0("fold", 1:n_folds)

# NAM parent information
parent_dir <- "Ames_NAM_hybrid/data/NAM/parents/"

NAM_info <- foreach(NAM_family=names(fold_list[["NAM/PHZ51"]]), .combine=rbind) %do% {
  
  NAM_parents <- read.table(paste0(parent_dir, NAM_family, ".txt"), header=FALSE, stringsAsFactors=FALSE)[, 1]
  
  NAM_parent <- sub("282set_", "", setdiff(NAM_parents, "282set_B73"))
  
  return(
    data.frame(parent=NAM_parent,
               is_tropical=(tolower(NAM_parent) %in% tolower(tropical_founders)),
               row.names=NAM_family)
    )
  
}

# Graphics
FS1 <- list()

FS1[[1]] <- ggplot(popInfo[!is.na(popInfo$cl), ], aes(x=PC1, y=PC2, colour=cl)) + 
  labs(x=NULL, y=paste0("PC 2 (", prop_var[2], " of genomic variance)"), colour="Population cluster") +
  facet_grid(~ tester, switch="y") +
  geom_point(size=2, alpha=0.6) +
  geom_point(size=2, alpha=0.6, shape=1) +
  geom_point(data=popInfo[fold_list[["Ames/PHZ51_B47"]][[1]], ], aes(x=PC1, y=PC2), colour="black", shape=1, size=3) +
  theme_bw(base_size=base_size) +
  theme(legend.position="bottom")


FS1[[2]] <- ggplot(popInfo[!is.na(popInfo$cl), ], aes(x=PC1, y=PC3, colour=cl)) + 
  labs(x=paste0("PC 1 (", prop_var[1], " of genomic variance)"), y=paste0("PC 3 (", prop_var[3], " of genomic variance)")) +
  facet_grid(~ tester, switch="y") +
  geom_point(size=2, alpha=0.6) +
  geom_point(size=2, alpha=0.6, shape=1) +
  geom_point(data=popInfo[fold_list[["Ames/PHZ51_B47"]][[1]], ], aes(x=PC1, y=PC3), colour="black", shape=1, size=3) +
  theme_bw(base_size=base_size) +
  theme(legend.position="none")


tiff(paste(output_dir, "FS1_revised.tiff", sep="/"), height=length(FS1)*u, width=1.6*u, res=300, compression="lzw")
multiplot(FS1, cols=1)
dev.off()

###############################################################################
# Figures
###############################################################################
#------------------------------------------------------------------------------
# F2: PCA
#------------------------------------------------------------------------------
#-------------------
# Parameters
#-------------------
# Sets
PCA_sets <- c("Ames/B47", "Ames/PHZ51", "NAM/PHZ51")
ref_lines <- c("282set_B73", "282set_Mo17", "282set_CML247")

# Files and directories
PCA.282set_file <- "Ames_NAM_hybrid/data/282set/PCA.rds"
Q.testers_file <- "Ames_NAM_hybrid/data/Hmp321/Q_testers.rds"
Q.inbreds_file <- "Ames_NAM_hybrid/data/Q_inbreds.rds"

#-------------------
# Script
#-------------------
# Population structure information
Q.inbreds <- data.frame(readRDS(Q.inbreds_file))
colnames(Q.inbreds) <- paste0("PC", 1:ncol(Q.inbreds))
Q.inbreds$label <- ""

PCA.282set <- readRDS(PCA.282set_file)
Q.282set <- data.frame(PCA.282set$u %*% diag(PCA.282set$d))
colnames(Q.282set) <- paste0("PC", 1:ncol(Q.282set))
Q.282set$label <- gsub("282set_", "", rownames(Q.282set))

Q.testers <- data.frame(readRDS(Q.testers_file))
colnames(Q.testers) <- paste0("PC", 1:ncol(Q.testers))
Q.testers$label <- gsub("282set_", "", rownames(Q.testers))

# Graphics
DF.PCA <- data.frame()

for (PCA_set in PCA_sets) {
  
  Q_DF <- data.frame(readRDS(paste(input_folders[PCA_set], "Q.rds", sep="/")))
  inbreds <- unique(gsub("/.+$", "", rownames(Q_DF)))
  testers <- unique(gsub(".+/", "", rownames(Q_DF)))
  Q_DF$label <- ""
  
  Q_DF <- rbind(Q_DF[, c(paste0("PC", 1:n_PCs), "label")],
                Q.inbreds[inbreds, c(paste0("PC", 1:n_PCs), "label")],
                Q.testers[testers, c(paste0("PC", 1:n_PCs), "label")])
  
  DF.PCA <- rbind(
    DF.PCA,
    cbind(Set=sub("_", "+", PCA_set),
          ID=rownames(Q_DF),
          Q_DF)
  )
  
}

DF.PCA$ID <- as.character(DF.PCA$ID)

DF.PCA$Genotype <- "Hybrid"
DF.PCA$Genotype[DF.PCA$ID %in% rownames(Q.testers)] <- "Tester"
DF.PCA$Genotype[DF.PCA$ID %in% rownames(Q.inbreds)] <- "Female"
DF.PCA$Genotype <- factor(DF.PCA$Genotype, levels=c("Female", "Hybrid", "Tester"))

DF.PCA$Set <- as.character(DF.PCA$Set)

DF.PCA <- DF.PCA[order(DF.PCA$Genotype), ]

F2 <- ggplot(DF.PCA, aes(x=PC1, y=PC2, colour=Genotype, shape=Genotype, size=Genotype, alpha=Genotype)) +
  labs(x=paste0("PC 1 (", prop_var[1], " of genomic variance)"), y=paste0("PC 2 (", prop_var[2], " of genomic variance)")) +
  geom_point() +
  geom_text(aes(label=label), size=4, vjust=-1.5, hjust=0.5, fontface="bold", show.legend = FALSE) +
  theme_bw(base_size=base_size) +
  scale_alpha_manual(values=alphas[levels(DF.PCA$Genotype)]) +
  scale_size_manual(values=sizes[levels(DF.PCA$Genotype)]) +
  scale_shape_manual(values=shapes[levels(DF.PCA$Genotype)]) +
  scale_color_manual(values=colours[levels(DF.PCA$Genotype)]) +
  facet_grid(. ~ Set, labeller=set_labeller) +
  theme(legend.position="bottom")

for (ref_line in ref_lines) {
  F2 <- F2 +
    annotate("point", x=Q.282set[ref_line, "PC1"], y=Q.282set[ref_line, "PC2"], size=2.5, shape=5) +
    annotate("text", x=Q.282set[ref_line, "PC1"], y=Q.282set[ref_line, "PC2"]+20,
             label=Q.282set[ref_line, "label"], vjust=0, hjust=0.5)
}

tiff(paste(output_dir, "F2_revised.tiff", sep="/"), height=1.1*u, width=2.7*u, res=300, compression="lzw")
print(F2)
dev.off()

#------------------------------------------------------------------------------
# F5: Genomic prediction between NAM and Ames
#------------------------------------------------------------------------------
suffix <- "_revision"

#-------------------
# Parameters
#-------------------
# Sets
TS_sets <- c("Ames/PHZ51_B47", "NAM/PHZ51")

# VC parameters
effect_type <- c("Ames/PHZ51_B47"="dom", "NAM/PHZ51"="add")

F5_features <- list("GWAS"=c("Unpartitioned", "GWAS"),
                    "var"=c("Gene"),
                    "var+gene"=c("GERP")
)

comparison_list <- list(
  "above"=list(
    c("Unpartitioned", "Gene"),
    c("Unpartitioned", "Gene+GERP")
  ),
  "below"=list(
    c("Unpartitioned", "GWAS")
  )
)

feature_list <- list(
  "GWAS"=setNames(F5_features[["GWAS"]], F5_features[["GWAS"]]),
  "var"=setNames(F5_features[["var"]], F5_features[["var"]]),
  "var+gene"=setNames(paste0("Gene+", F5_features[["var+gene"]]), F5_features[["var+gene"]])
)

TS_legend <- c("Ames/PHZ51_B47"="Ames-H ⇒ NAM-H",
               "NAM/PHZ51"="NAM-H ⇒ Ames-H")

y_shift <- setNames(c(0.06, 0.06), TS_legend)

#-------------------
# Script
#-------------------
# GS results by set
DF.GS <- data.frame()

for (TS_set in TS_sets) {
  
  VS_set <- setdiff(TS_sets, TS_set)
  
  # Files
  TS_folder <- paste("Ames_NAM_hybrid/data", TS_set, sep="/")
  VS_folder <- paste("Ames_NAM_hybrid/data", VS_set, sep="/")
  
  # Training set data
  Y.TS <- readRDS(paste(TS_folder, "Y.rds", sep="/"))
  TS <- rownames(Y.TS)
  
  LMM <- readRDS(paste0(TS_folder, "/LMM", suffix, ".rds"))
  VC <- readRDS(paste0(TS_folder, "/VC", suffix, ".rds"))
  Ypred <- readRDS(paste0(TS_folder, "/Ypred", suffix, ".rds"))
  
  # Validation set data
  Y.VS <- readRDS(paste(VS_folder, "Y.rds", sep="/"))
  VS <- rownames(Y.VS)
  
  for (study in names(feature_list)) {
    
    features <- names(feature_list[[study]])
    
    # GS accuracy
    for (trait in colnames(Y.VS)) {
      
      yobs <- setNames(Y.VS[, trait], rownames(Y.VS))
      
      ypred <- Ypred[[effect_type[TS_set]]][[study]][[trait]]
      colnames(ypred) <- feature_legend[colnames(ypred)]
      
      tested_features <- colnames(ypred)[colnames(ypred) %in% features & apply(ypred, 2, function(x) any(is.finite(x)))]
      
      DF.GS <- rbind(DF.GS,
                     foreach(feature=tested_features, .combine=rbind) %:%
                       foreach(fold=names(fold_list[[VS_set]]), .combine=rbind) %do% {
                         
                         VS.fold=fold_list[[VS_set]][[fold]]
                         
                         r <- cor(yobs[VS.fold], ypred[VS.fold, feature], use="complete")
                         
                         data.frame(study=study,
                                    set=TS_legend[TS_set],
                                    trait=trait,
                                    feature=feature_list[[study]][feature],
                                    fold=fold,
                                    r=r)
                         
                       })
      
    }
    
  }
  
}

# Figure
annotation_df <- foreach(set=levels(DF.GS$set), .combine=rbind) %:%
  foreach(trait=traits, .combine=rbind) %:%
  foreach(comparison_type=names(comparison_list), .combine=rbind) %do% {
    
    comparisons <- comparison_list[[comparison_type]]
    
    foreach(j=1:length(comparisons), .combine=rbind) %do% {
      
      comp <- comparisons[[j]]
      
      tmp <- DF.GS[DF.GS$set == set & DF.GS$trait == trait, ]
      
      if (all(comp %in% tmp$feature)) {
        
        # Significance of difference
        test <- t.test(tmp$r[tmp$feature == comp[2]], tmp$r[tmp$feature == comp[1]], paired=TRUE)
        stopifnot(all(tmp$fold[tmp$feature == comp[2]] == tmp$fold[tmp$feature == comp[1]]))
        
        p <- ifelse(is.finite(test$p.value), test$p.value, 1)
        
        # r.pval <- ifelse(is.finite(p), p.labels[as.character(cut(p, breaks=p.classes))], "")
        r.pval <- sub("-0", "-", sub("e", "E", ifelse(p <= 0.1, paste0("p=", signif(p, 2)), "")))
        
        # Y-coordinate
        if (comparison_type == "below") {
          y <- min(tmp$r, na.rm=TRUE) - y_shift[set]
        } else {
          y <- max(tmp$r, na.rm=TRUE) + y_shift[set] * j
        }
        
        # Output
        data.frame(set=set, 
                   trait=trait,
                   type=comparison_type,
                   start=comp[1],
                   end=comp[2],
                   diff=test$estimate,
                   y=y,
                   label=r.pval)
        
      }
      
    }
    
  }

annotation_df$diff_type <- "0"
annotation_df$diff_type[annotation_df$diff <= 0 & annotation_df$label != ""] <- "-1"
annotation_df$diff_type[annotation_df$diff > 0 & annotation_df$label != ""] <- "+1"

mean.GS <- dplyr::group_by(DF.GS, set, trait, feature) %>%
  dplyr::summarise(r=mean(r)) %>%
  data.frame

F5 <- ggplot(DF.GS, aes(x=feature, y=r)) +
  geom_jitter(width=0.1, alpha=1, shape=1, colour="#3399FF") +
  geom_boxplot(fill=NA, colour="#000088", width=0.5, outlier.shape=NA) +
  geom_point(data=mean.GS, shape=18, colour="#000000", size=3.5) +
  labs(y="Validation scheme", title="") +
  theme_bw(base_size=18) +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_text(angle=90, size=16, vjust=0.5, hjust = 1),
        axis.ticks.x=element_blank(),
        plot.title = element_text(hjust = 0, size=base_size),
        legend.position = "bottom") +
  guides(fill=guide_legend(nrow=1, byrow=TRUE)) +
  scale_x_discrete(breaks=levels(DF.GS$feature), labels=levels(DF.GS$feature)) +
  facet_grid(set ~ trait, scales ="free", switch="y") +
  geom_signif(data=annotation_df[annotation_df$type == "above", ],
              aes(xmin=start, xmax=end, annotations=label, y_position=y, colour=diff_type),
              manual=TRUE, tip_length=0.02, textsize=3.8, vjust=0,
              show.legend=FALSE) +
  geom_signif(data=annotation_df[annotation_df$type == "below", ],
              aes(xmin=start, xmax=end, annotations=label, y_position=y, colour=diff_type),
              manual=TRUE, tip_length=-.02, textsize=3.8, vjust=2,
              show.legend=FALSE)

tiff(paste(output_dir, "F5_revised.tiff", sep="/"), height=1.5*u, width=2*u, res=300, compression="lzw")
print(F5)
dev.off()

#------------------------------------------------------------------------------
# TS9: Prediction accuracy from different Ames subsets with equal sample size
#------------------------------------------------------------------------------
#-------------------
# Parameters
#-------------------
# VC parameters
effect_type <- c("Ames/PHZ51"="add", "Ames/B47"="add")
TS_sets <- names(effect_type)

VS_set <-"NAM/PHZ51"

features <- list(
  "Ames/PHZ51_B47"=c("Unpartitioned"),
  "Ames/PHZ51"=c("Unpartitioned"),
  "Ames/B47"=c("Unpartitioned")
)

baseline <- "Ames/PHZ51"

suffix_names <- c("_282set"="Common lines", "_subsets"="Random subsets")

#-------------------
# Script
#-------------------
# GS results by set
DF.GS <- data.frame()

for (TS_set in TS_sets) {
  
  # Files
  TS_folder <- paste("Ames_NAM_hybrid/data", TS_set, sep="/")
  VS_folder <- paste("Ames_NAM_hybrid/data", VS_set, sep="/")
  
  # Training set data
  Y.TS <- readRDS(paste(TS_folder, "Y.rds", sep="/"))
  TS <- rownames(Y.TS)
  
  # Validation set data
  Y.VS <- readRDS(paste(VS_folder, "Y.rds", sep="/"))
  VS <- rownames(Y.VS)
  
  for (suffix in names(suffix_names)) {
    
    # Predictions
    Ypred <- readRDS(paste0(TS_folder, "/Ypred", suffix, ".rds"))
    
    # GS accuracy
    for (trait in colnames(Y.VS)) {
      
      yobs <- setNames(Y.VS[, trait], rownames(Y.VS))
      
      ypred <- Ypred[[effect_type[TS_set]]][["var"]][[trait]]
      
      DF.GS <- rbind(DF.GS,
                     foreach(fold=names(fold_list[[VS_set]]), .combine=rbind) %do% {
                       
                       VS.fold=fold_list[[VS_set]][[fold]]
                       
                       foreach(j=1:ncol(ypred), .combine=rbind) %do% {
                         
                         r <- cor(yobs[VS.fold], ypred[VS.fold, j], use="complete")
                         
                         data.frame(
                           sampling=suffix_names[suffix],
                           set=TS_set,
                           trait=trait,
                           family=fold,
                           is_tropical=NAM_info[fold, "is_tropical"],
                           r=r,
                           rep=j
                           )
                         
                       }
                       
                     })
      
    }
    
  }
  
}

DF.GS$set <- factor(DF.GS$set, levels=c(baseline, setdiff(unique(DF.GS$set), baseline)))
DF.GS$rep <- factor(DF.GS$rep)

# Table
TS9 <- foreach(trait=traits, .combine=rbind) %do% {
  
  trait_DF <- DF.GS[DF.GS$trait == trait, ]
  trait_DF$instance <- factor(paste(trait_DF$set, trait_DF$family, sep="_"))
  
  NAM_subsets <- list(
    "All origins" = unique(trait_DF$family),
    "Non-tropical origin" = unique(trait_DF[! trait_DF$is_tropical, "family"]),
    "Tropical origin" = unique(trait_DF[trait_DF$is_tropical, "family"])
  )
  
  foreach(sampling=unique(trait_DF$sampling), .combine=rbind) %:% 
    foreach(subset_name=names(NAM_subsets), .combine=merge) %do% {
      
      tmp_DF <- trait_DF[trait_DF$sampling == sampling & trait_DF$family %in% NAM_subsets[[subset_name]], ]
      
      if (length(unique(tmp_DF$rep)) > 1) {
        
        fit <- aov(r ~ set + family + Error(instance) + set:rep, data=tmp_DF)
        
        coef.val <- formatC(coef(fit)[["instance"]][1], n_digits, format="f")
        p.val <- signif(summary(fit)[["Error: instance"]][[1]]["set", "Pr(>F)"], 2)
        
      } else {
        
        fit <- aov(r ~ set + family, data=tmp_DF)
        
        coef.val <- formatC(coef(fit)[2], n_digits, format="f")
        p.val <- signif(summary(fit)[[1]]["set", "Pr(>F)"], 2)
        
      }
      
      r_diff <- paste0(coef.val, " (", p.val, ")")
      
      setNames(data.frame(sampling, trait, r_diff), c("Sampling", "Trait", subset_name))
      
    }
  
}

TS9 <- TS9[order(TS9$Trait, TS9$Sampling), ]

# Output
write.table(TS9, paste(output_dir, "TS9.csv", sep="/"), sep=",", row.names=FALSE, quote=FALSE)
