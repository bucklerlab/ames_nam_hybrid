#!/usr/bin/env bash

home_dir=/workdir/gr226/Ames_NAM_hybrid/
input_dir=/workdir/gr226/Ames_NAM_hybrid/data/

input_prefix=${input_dir}NAM/ZeaGBSv27_AGPv4_NAM_chr
output_prefix=${input_dir}NAM/AGPv4_NAM_chr

ref_prefix=${input_dir}NAM/AGPv4_parents_chr

beagle_version=beagle.03Jul18.40b.jar
tassel=${home_dir}tassel-5-standalone/run_pipeline.pl

n_chromosomes=10

checkpoint=1

if [ ! -d ${home_dir}tmp ]; then mkdir ${home_dir}tmp; fi


###################################################################
# 1. Extracting information from imputed files
###################################################################
if [ ${checkpoint} -le 1 ]
then

cd ${home_dir}tmp
echo Extracting imputation statistics from imputed files...

echo -e "CHROM\tPOS\tAF_NAM\tR2_Beagle" > ${input_dir}NAM/NAM_imputation_stats.txt

for i in $(seq 1 1 ${n_chromosomes})
do

bcftools query -f '%CHROM\t%POS\t%AF\t%DR2\n' ${output_prefix}${i}.imputed.vcf.gz >> ${input_dir}NAM/NAM_imputation_stats.txt

done

fi


###################################################################
# 2. Masking genotype files
###################################################################
if [ ${checkpoint} -le 2 ]
then

cd ${home_dir}
echo Masking genotype files...

for i in $(seq 1 1 ${n_chromosomes})
do

${tassel} -Xmx120g -MaskGenotypesPlugin -inputFile ${input_prefix}${i}.recode.vcf.gz -outputFile ${input_prefix}${i}.masked.vcf.gz

bcftools query -f '%CHROM\t%POS\n' ${input_prefix}${i}.masked.vcf.gz > ${input_prefix}${i}.masked.pos

done

fi
 

###################################################################
# 3. Imputing genotype files
###################################################################
if [ ${checkpoint} -le 3 ]
then

cd ${home_dir}
echo Imputed masked genotype files...

for i in $(seq 1 1 ${n_chromosomes})
do

ref_file=${ref_prefix}${i}.recode.vcf.gz
input_file=${input_prefix}${i}.masked.vcf.gz
out=${output_prefix}${i}.masked.imputed

java -Xmx120g -jar ${beagle_version} ref=${ref_file} gt=${input_file} burnin=10 iterations=15 ne=1000 out=${out}

done

fi


###################################################################
# 4. Subsetting on positions in masked file
###################################################################
if [ ${checkpoint} -le 4 ]
then

cd ${home_dir}tmp
echo Subsetting on positions in masked file...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

out=${output_prefix}${i}.masked.imputed

vcftools --gzvcf ${out}.vcf.gz \
--out ${out} \
--positions ${input_prefix}${i}.masked.pos \
--recode &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${output_prefix}${i}.masked.imputed.recode.vcf &

done
wait

fi


###################################################################
# 5. Estimating imputation accuracy
###################################################################
if [ ${checkpoint} -le 5 ]
then

cd ${home_dir}tmp
echo Estimating imputation accuracy...

for i in $(seq 1 1 ${n_chromosomes})
do

${tassel} -Xmx120g \
-fork1 -importGuess ${input_prefix}${i}.recode.vcf.gz \
-fork2 -importGuess ${input_prefix}${i}.masked.vcf.gz \
-fork3 -importGuess ${output_prefix}${i}.masked.imputed.recode.vcf.gz \
-combine4 -input1 -input2 -input3 \
-ImputationAccuracyPlugin -endPlugin \
-export ${output_prefix}${i}.imputation_accuracy.txt

done

fi
