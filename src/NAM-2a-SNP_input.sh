#!/usr/bin/env bash

input_dir=/workdir/gr226/Ames_NAM_hybrid/data/NAM/

n_chromosomes=10

checkpoint=1

if [ ! -d ${input_dir}tmp/ ]
then
mkdir ${input_dir}tmp/
fi

cd ${input_dir}tmp/


###################################################################
# 1. Renaming samples in VCF
###################################################################
if [ ${checkpoint} -le 1 ]
then

echo Renaming samples...

# Renaming samples
bcftools query -l ${input_dir}ZeaGBSv27_20171204_AGPv4_NAM.vcf | sed 's/\:.*//g;s/(.*//g' > GBS_names.txt
bcftools reheader -s GBS_names.txt -o ${input_dir}ZeaGBSv27_AGPv4_NAM.vcf ${input_dir}ZeaGBSv27_20171204_AGPv4_NAM.vcf

fi

###################################################################
# 2. Converting input to VCF by chromosome
###################################################################
if [ ${checkpoint} -le 2 ]
then

echo Converting...

# Converting
~/tassel-5-standalone/run_pipeline.pl -Xmx200g -vcf ${input_dir}ZeaGBSv27_AGPv4_NAM.vcf \
-separate -sortPositions \
-export ${input_dir}ZeaGBSv27_AGPv4_NAM_chr -exportType VCF

fi

###################################################################
# 3. Indexing VCF
###################################################################
if [ ${checkpoint} -le 3 ]
then

echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

# Indexing
input_NAM=${input_dir}ZeaGBSv27_AGPv4_NAM_chr${i}

bgzip -f ${input_NAM}.vcf
tabix -f -p vcf ${input_NAM}.vcf.gz

done

fi

