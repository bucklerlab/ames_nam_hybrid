##############################################################
# Estimation of BLUEs in NAM
##############################################################
#-------------------------------------------------------------
# Script parameters
#-------------------------------------------------------------
# Working directory
if (Sys.info()["nodename"] == "rs-btgr226dt") {
  wd <- "/home/gr226/Documents"
  src_file <- "/home/gr226/Projects/Ames_NAM_hybrid/src/_utils.R"
} else {
  wd <- "/workdir/gr226"
  src_file <- "/workdir/gr226/Ames_NAM_hybrid/src/_utils.R"
}

dir.create(wd, showWarnings = FALSE)
setwd(wd)

# Phenotypic file
pheno_file <- "Ames_NAM_hybrid/data/NAM/NamTC_asreml160422.csv"

# Traits whose BLUEs to estimate
selected_traits <- c("DTS", "PH", "GY")
covariates <- list(NULL, NULL, c("DTS"))

# Output
MMA.BLUE_file <- "Ames_NAM_hybrid/data/NAM/BLUE_analysis.rds"
BLUE.summary_file <- "Ames_NAM_hybrid/data/NAM/BLUE_summary.csv"
BLUE_file <- "Ames_NAM_hybrid/data/NAM/BLUEs.csv"

MMA.BLUP_file <- "Ames_NAM_hybrid/data/NAM/BLUP_analysis.rds"
BLUP.summary_file <- "Ames_NAM_hybrid/data/NAM/BLUP_summary.csv"
trait.summary_file <- "Ames_NAM_hybrid/data/NAM/Trait_summary.csv"
BLUP_file <- "Ames_NAM_hybrid/data/NAM/BLUPs.csv"
r2_file <- "Ames_NAM_hybrid/data/NAM/r2.csv"

#-------------------------------------------------------------
# Functions
#-------------------------------------------------------------
library(asreml)
library(ggplot2)
source(src_file)

#-------------------------------------------------------------
# Raw data
#-------------------------------------------------------------
# Input
pheno <- read.csv(pheno_file)
colnames(pheno)[colnames(pheno) == "Yield"] <- "GY"

# Formatting
for (variable in c("Order", "field", "pblock", "pop", "entrynum", "env")) {
  pheno[, variable] <- as.factor(pheno[, variable])
}
pheno$row_factor <- as.factor(pheno$row)
pheno$range_factor <- as.factor(pheno$range)

pheno$env_field <- as.factor(pheno$env):as.factor(pheno$field)

# Genotype names
pheno$genotype <- as.character(pheno$female_pedigree)
pheno$genotype <- paste(pheno$genotype, "PHZ51", sep="/")

# Location variable
pheno$loc <- as.factor(substr(as.character(pheno$env_name), 3, 4))

#-------------------------------------------------------------
# Description
#-------------------------------------------------------------
n_traits <- length(selected_traits)

# Structure by env
n_env <- numeric(length(selected_traits))
names(n_env) <- selected_traits

for (trait in selected_traits) {
  
  cat(paste(sep="", "--------------\n", trait, "\n--------------\n"))
  
  tmp <- pheno[!is.na(pheno[,trait]), ]
  
  print( tab <- table(tmp$field, tmp$env_name) )
  
  n_env[trait] <- ncol(tab)
  
}

# Histograms & Boxplots
pdf("Ames_NAM_hybrid/data/NAM/Descriptive_statistics.pdf")

for (trait in selected_traits) {
  
  hist(pheno[,trait], main=trait, xlab="Values")
  
  tmp <- data.frame(pop=pheno$pop,
                    trait=pheno[,trait])
  
  tmp$pop <- factor(tmp$pop, levels=levels(pheno$pop)[order(tapply(pheno[,trait], pheno[,"pop"], mean, na.rm=TRUE))])
  
  plot(
    ggplot(data=tmp, aes(x=pop, y=trait, fill=pop)) + geom_boxplot() +
      labs(x="Population", y=trait, fill="Pop.") +
      theme_bw(base_size=24) + theme(axis.text.x = element_text(angle=45, size = 9, vjust=0.5), legend.position="none")
  )
  
}

dev.off()

#-------------------------------------------------------------
# Estimating BLUEs
#-------------------------------------------------------------
# Mixed-model analysis
MMA.BLUE <- getBLUEs(pheno.data=pheno,
                traits=selected_traits,
                covariates=covariates,
                s1.model="idv(env_field):ar1(row_factor):ar1(range_factor)",
                pdf.out="Ames_NAM_hybrid/data/NAM/Model_diagnostics.pdf")

saveRDS(MMA.BLUE, MMA.BLUE_file)

# Model summary
write.csv(MMA.BLUE$model.summary, BLUE.summary_file, row.names=FALSE)

# Genotype BLUEs
write.csv(MMA.BLUE$BLUE, BLUE_file, row.names=FALSE)

#-------------------------------------------------------------
# Estimating BLUPs
#-------------------------------------------------------------
MMA.BLUP <- getBLUPs(pheno.data=pheno,
                     traits=selected_traits,
                     covariates=covariates,
                     s1.model="idv(env_field):ar1(row_factor):ar1(range_factor)",
                     pdf.out="Ames_NAM_hybrid/data/NAM/Model_diagnostics-BLUP.pdf")

saveRDS(MMA.BLUP, MMA.BLUP_file)

# Model summary
write.csv(MMA.BLUP$model.summary, BLUP.summary_file, row.names=FALSE)

# Trait summary
write.csv(MMA.BLUP$trait.summary, trait.summary_file, row.names=FALSE)

# Genotype BLUPs
write.csv(MMA.BLUP$BLUP, BLUP_file, row.names=FALSE)

# BLUP reliabilities
write.csv(MMA.BLUP$r2, r2_file, row.names=FALSE)
