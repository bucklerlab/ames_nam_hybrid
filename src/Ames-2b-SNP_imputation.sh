#!/usr/bin/env bash

home_dir=/workdir/gr226/Ames_NAM_hybrid/
cd ${home_dir}

input_dir=${home_dir}data/

Hmp_prefix=${input_dir}Hmp321/hmp321_282_agpv4_merged_chr

Ames_prefix=${input_dir}Ames/AGPv4_Ames_chr

beagle_version=beagle.03Jul18.40b.jar

n_chromosomes=10

checkpoint=1


###################################################################
# 1. Format data in Ames
###################################################################
if [ ${checkpoint} -le 1 ]
then

# Filtering markers
echo Filtering markers in Ames...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools annotate -x ^FORMAT/GT -e 'REF~"N" | ALT~"N"' -Ov -o ${Ames_prefix}${i}.vcf ${Ames_prefix}${i}.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

sed -i '/^#/! s/0\/1/\.\/\./g; s/1\/0/\.\/\./g' ${Ames_prefix}${i}.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --vcf ${Ames_prefix}${i}.vcf \
--out ${Ames_prefix}${i} \
--positions ${Hmp_prefix}${i}.imputed.pos \
--min-alleles 2 \
--max-alleles 2 \
--recode &

done
wait

# Indexing
echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${Ames_prefix}${i}.recode.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${Ames_prefix}${i}.recode.vcf.gz &

done
wait

fi


###################################################################
# 2. Imputation in Ames
###################################################################
if [ ${checkpoint} -le 2 ]
then

echo Imputing in Ames...

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}

ref=${Hmp_prefix}${i}.imputed
study=${Ames_prefix}${i}.recode
out=${Ames_prefix}${i}.imputed

java -Xmx120g -jar ${beagle_version} ref=${ref}.vcf.gz gt=${study}.vcf.gz burnin=10 iterations=15 ne=1000 out=${out}

tabix -f -p vcf ${out}.vcf.gz &

done

fi
