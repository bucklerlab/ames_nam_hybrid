#!/usr/bin/env bash

output_dir=/workdir/gr226/Ames_NAM_hybrid/data/282set/
if [ ! -d ${output_dir} ]; then mkdir -p ${output_dir}tmp; fi

output_prefix=${output_dir}AGPv4_282set_chr

Hmp_prefix=/workdir/gr226/Ames_NAM_hybrid/data/Hmp321/hmp321_282_agpv4_merged_chr

map_file=/workdir/gr226/Ames_NAM_hybrid/data/selected_SNPs.txt

n_chromosomes=10

###################################################################
# Subsetting on 282 set in imputed Hapmap3.2.1 file
###################################################################
echo Subsetting VCF by samples and SNPs...

bcftools query -l ${Hmp_prefix}1.imputed.vcf.gz | grep 282set > ${output_dir}282set_samples.txt

# Subsetting on 282set samples
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

grep -P "^${i}\t" ${map_file} > ${output_dir}selected_SNPs_chr${i}.txt

vcftools --gzvcf ${Hmp_prefix}${i}.imputed.vcf.gz \
--out ${output_prefix}${i} \
--keep ${output_dir}282set_samples.txt \
--positions ${output_dir}selected_SNPs_chr${i}.txt \
--recode &

done
wait

echo Compressing files...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

rm ${output_dir}selected_SNPs_chr${i}.txt

bgzip -f ${output_prefix}${i}.recode.vcf &

done
wait


echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${output_prefix}${i}.recode.vcf.gz &

done
wait

