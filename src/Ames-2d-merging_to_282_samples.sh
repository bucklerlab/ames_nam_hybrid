#!/usr/bin/env bash

input_dir=/workdir/gr226/Ames_NAM_hybrid/data/Ames/
input_prefix=${input_dir}AGPv4_Ames_chr

Hmp_prefix=/workdir/gr226/Ames_NAM_hybrid/data/Hmp321/hmp321_282_agpv4_merged_chr

n_chromosomes=10

# Subset of 282 samples in Ames panel
awk -F" " '{print $1}' ${input_dir}282set_samples.txt > ${input_dir}282set_samples.hmp321.txt


###################################################################
# 1. Subsetting Hapmap files on 282 samples
###################################################################
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --gzvcf ${Hmp_prefix}${i}.imputed.vcf.gz \
--out ${input_dir}AGPv4_282_subset_chr${i} \
--keep ${input_dir}282set_samples.hmp321.txt \
--recode &

done
wait


###################################################################
# 2. Renaming sample names as in Ames panel
###################################################################
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools reheader -s ${input_dir}282set_samples.txt -o ${input_dir}AGPv4_282_subset_chr${i}.renamed.vcf ${input_dir}AGPv4_282_subset_chr${i}.recode.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

rm ${input_dir}AGPv4_282_subset_chr${i}.recode.vcf

bgzip -f ${input_dir}AGPv4_282_subset_chr${i}.renamed.vcf

tabix -f -p vcf ${input_dir}AGPv4_282_subset_chr${i}.renamed.vcf.gz &

done
wait


###################################################################
# 3. Merging data from Ames and 282 samples
###################################################################
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools merge -m all -Oz -o ${input_prefix}${i}.augmented.imputed.vcf.gz --threads 4 ${input_prefix}${i}.imputed.vcf.gz ${input_dir}AGPv4_282_subset_chr${i}.renamed.vcf.gz &

done
wait
